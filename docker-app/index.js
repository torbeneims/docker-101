// Import the express module
const express = require('express');

// Create our app instance
const app = express();

// Register a new GET-endpoint to listen on the root path (/)
app.get('/', (request, response) => {
    response.send(`Hello World!`);
});

// Start the app
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Express Hello World! Listening on port ${port}`);
})
