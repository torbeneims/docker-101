Let's start by creating a simple Node app. This process we will be using to dockerize it should more or less be the same 
for all your Node backends.

We will be using express, an easy to use REST-API framework.

First, create a new Node project
```shell
mkdir docker-app && cd docker-app
npm init
```

and install
* express, which we will use to create our REST-endpoints
* cors, which we need to specify the CORS header allowing us to make requests from other servers (https://youtu.be/4KHiSt0oLJ0?t=80s)
```shell
npm install express --save
```

Now create a new `ìndex.js` file and let express respond to incoming requests with a simple text.
````js
// Import the express module
const express = require('express');

// Create our app instance
const app = express();

// Register a new GET-endpoint to listen on the root path (/)
app.get('/', (request, response) => {
    response.send(`Hello World!`);
});

// Start the app
const port = 80;
app.listen(port, () => {
    console.log(`Express Hello World! Listening on port ${port}`);
})
````

We can now test out our app by running
```shell
node index.js
```
and visiting [localhost:3000](http://localhost:3000)

Also add this command to your scripts inside the `package.json` file:
```json
{
  "scripts": {
    "run": "node index.js"
  }
}
```

Add the `docker-compose.yml`, `Dockerfile` and `.dockerignore` files.
`docker-compose.yml`
```yml
services:
  my-node-app:
    container_name: my-node-app
    restart: unless-stopped
    working_dir: /usr/src/app
    ports:
      # external port (what a user sees): internal port (what your app uses)
      - 80:3000
    volumes:
      - .:/usr/src/app  # make the current directory (.) available under /usr/src/app inside our container
      - ./node_modules  # a volume to install our node_modules in
    build: .
```

`Dockerfile`
```Dockerfile
FROM node:alpine

WORKDIR /usr/src/app

RUN npm install

# Expose the port to the outside docker-environment (but not our system yet)
EXPOSE 3000

# Run
CMD ["npm", "run", "run"]
```

`.dockerignore` (This will prevent docker from loading our local node_modules into the container)
```ignorelang
node_modules
```

Taking it a step further, we can add CORS to our express app to be able to make request to it from other servers:
```shell
npm install cors --save
```
Add this directly after `const app...`
```js
const cors = require('cors');

app.use(cors({origin: '*'}));
```

You can also use environment variables local in your docker container:
```js
const port = process.env.PORT || 3000;
```



